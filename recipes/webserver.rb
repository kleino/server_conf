#node.default['nginx']['user'] = 'root'
#node.default['nginx']['worker_processes'] = '200'




#application from git
#default['app']['web_dir'] = '/var/data/www/apps/tomatoes'

#git "#{node.app.web_dir}" do



include_recipe "nginx"

# https://github.com/miketheman/nginx/issues/248
node.default['nginx']['pid'] = '/run/nginx.pid'


#??
#node.default['nginx']['listen_ports'] = %w(80)
#node.default['nginx']['default_site_enabled']

#NGINX

#Create necessary app folders
%w(public logs).each do |dir|
	directory "#{node.app.web_dir}/#{dir}" do
		owner node.user.name
		mode "0755"
		recursive true
	end
end


#CCreate 
template "#{node.nginx.dir}/sites-available/#{node.app.name}.conf" do
	source "nginx.conf.erb"
	mode "0644"
	notifies :reload, "service[nginx]"
end

nginx_site "#{node.app.name}.conf"

# deploy your sites configuration from the files/ driectory in your cookbook
#cookbook_file "#{node.app.web_dir}/public/index.html" do
#	source "index.html"
#	owner node.user.name
#	#owner "root"
#	#group "root"
#	mode 0755
#end

#cookbook_file "#{node.app.web_dir}/application.py" do
#	source "application.py"
#	owner node.user.name
#	#owner "root"
#	#group "root"
#	mode 0755
#end

#UWSGI
template "#{node.app.web_dir}/#{node.app.name}_uwsgi.ini" do
	source "uwsgi.ini.erb"
	owner node.user.name
	#owner "root"
	#group "root"
	mode "0644"
end




#{node.app.env_dir}

command = "#{node.app.env_dir}/bin/uwsgi #{node.app.web_dir}/#{node.app.name}_uwsgi.ini"
supervisor_service "uwsgi_app" do
	command command
	environment "BANKSTUFF_ENV" => "prod"
end

	#action [:enable, :start] #is the default




# enable your sites configuration using a definition from the nginx cookbook
#nginx_site "example.com" do
#  enable true
#end