directory "/home/vagrant/.ssh" do
  mode 00755
  owner "vagrant"
end

cookbook_file "/home/vagrant/.ssh/config" do
  source "ssh_config"
  owner "vagrant"
  mode 00644
end