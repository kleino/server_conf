#
# Cookbook Name:: bankstuff_srv
# Recipe:: default
#
# Copyright (C) 2015 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "bankstuff_srv::base"
include_recipe "bankstuff_srv::database"
include_recipe "bankstuff_srv::python"
include_recipe "bankstuff_srv::webserver"