package 'libpq-dev' do
  action :install
end


#include_recipe 'postgresql::ruby'
#include_recipe 'postgresql::server'

include_recipe "database::postgresql"


conn_info = {
	:host      => '127.0.0.1',
    :port      => 5432,
    :username  => 'postgres',
    :password  => 'postgresq!'
    #:password  => node['postgresql']['password']['postgres']
}

# create a postgresql database
postgresql_database "#{node.app.db.name}" do
  connection conn_info
  action :create
end

postgresql_database_user "#{node.app.db.user}" do
  connection conn_info
  password node.app.db.password # or "#{node.app.db.password}" but not with ''
  privileges [:all]
  database_name node.app.db.name
  action [:create] #drop user everytime and recreate him during all provisioning runs
end