#python-dev for uwsgi, nodejs for extjs
packages = ['python3-dev', 'nodejs']

packages.each do |pack|
	package pack do
		action :install
	end
end

include_recipe "python"
include_recipe "pyenv::system"

pyenv_python "3.4.2"
#pyenv_rehash 'Doing the rehash dance'



# create a Python 3.4.2 virtualenv
python_virtualenv "#{node.app.web_dir}/app_env" do
  interpreter "/usr/local/pyenv/versions/3.4.2/bin/python3"
  owner "vagrant"
  group "vagrant"
  action :create
end

python_pip "#{node.app.web_dir}/requirements.txt" do
  virtualenv "#{node.app.web_dir}/app_env"
  options '-r'
  action :install
end

#/usr/local/pyenv/versions/3.4.2/python3