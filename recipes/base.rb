user 'kleino' do
	home "/home/kleino"
	shell "/bin/bash"
	supports manage_home: true
	password '$1$lol$kZIzKrPGnrjPMAGZDj89a/'
	action :create
end

directory "/home/vagrant/.ssh" do
  mode 00755
  owner "vagrant"
end

cookbook_file "/home/vagrant/.ssh/config" do
  source "ssh_config"
  owner "vagrant"
  mode 00644
end


#bash "give group sudo privileges" do
#  code <<-EOH
#    sed -i '/%devs.*/d' /etc/sudoers
#    echo '%#{node['group']} ALL=(ALL) ALL' >> /etc/sudoers
#  EOH
#  not_if "grep -xq '%devs ALL=(ALL) ALL' /etc/sudoers"
#end


#Create app folder
directory "#{node.app.web_dir}" do
	owner node.user.name
  group node.user.name
	mode "0755"
	recursive true
end


#TODO: add fingerprint
#Checkout project from git
git "#{node.app.web_dir}" do
  repository "git@bitbucket.org:kleino/bankstuff.git"
  reference "master"
  action :sync
  user "vagrant"
end

magic_shell_environment "DB_URL" do
  value "postgresql://#{node.app.db.user}:#{node.app.db.password}@127.0.0.1/#{node.app.db.name}"
end

