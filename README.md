# Sample bankstuff_srv-cookbook

* ubuntu-14.04
* Postgresql
* supervisor
* nginx
* flaskapp + python3

# Usage
1) Checkout from git
```
git checkout
```
2) Alter "attributes/default.rb"

3) Make sure that deployment keys are set up correctly for bitbucket & github. For windows, loading the keys into pagent is needed

4) Install cookbook dependencies with [berkshelf](http://berkshelf.com/)
```
berks install
```
5) Start up vagrant and provision the box. This may take a while
```
vagrant up
```
6) When completed successfully, you should see your app at http://127.0.0.1:8080

Tested on Windows7 64bit

# TODO:
* Password handling
* User management