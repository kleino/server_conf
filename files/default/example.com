server {
  listen 80;

  server_name mywebsite.com;

  root "/var/www/myapp";
}