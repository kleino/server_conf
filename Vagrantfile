# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = '2'

Vagrant.require_version '>= 1.5.0'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  config.vm.hostname = 'bankstuff-srv-berkshelf'

  # Set the version of chef to install using the vagrant-omnibus plugin
  # NOTE: You will need to install the vagrant-omnibus plugin:
  #
  #   $ vagrant plugin install vagrant-omnibus
  #
  if Vagrant.has_plugin?("vagrant-omnibus")
    config.omnibus.chef_version = 'latest'
  end

  #Workaround in case of "shared folders are missing" error
  config.trigger.before [:reload, :halt], stdout: true do 
    `rm .vagrant\\machines\\default\\virtualbox\\synced_folders`
  end
  # Every Vagrant virtual environment requires a box to build off of.
  # If this value is a shorthand to a box in Vagrant Cloud then
  # config.vm.box_url doesn't need to be specified.
  config.vm.box = 'chef/ubuntu-14.04'

  #Forward local ssh keys to box
  config.ssh.private_key_path = 'C:\Users\Kleino\.ssh\id_rsa'
  config.ssh.forward_agent = true

  # Assign this VM to a host-only network IP, allowing you to access it
  # via the IP. Host-only networks can talk to the host machine as well as
  # any other machines on the same network, but cannot be accessed (through this
  # network interface) by any external networks.
  config.vm.network :private_network, type: 'dhcp'

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network "forwarded_port", guest: 80, host: 8080

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "../shared", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider :virtualbox do |vb|
  #   # Don't boot with headless mode
  #   vb.gui = true
  #
  #   # Use VBoxManage to customize the VM. For example to change memory:
  #   vb.customize ["modifyvm", :id, "--memory", "1024"]
  # end
  #
  # View the documentation for the provider you're using for more
  # information on available options.

  # The path to the Berksfile to use with Vagrant Berkshelf
  # config.berkshelf.berksfile_path = "./Berksfile"

  # Enabling the Berkshelf plugin. To enable this globally, add this configuration
  # option to your ~/.vagrant.d/Vagrantfile file
  config.berkshelf.enabled = true

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to exclusively install and copy to Vagrant's shelf.
  # config.berkshelf.only = []

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to skip installing and copying to Vagrant's shelf.
  # config.berkshelf.except = []


  config.vm.provision :chef_solo do |chef|
	chef.add_recipe "apt"
	chef.add_recipe "build-essential"
	chef.add_recipe "git"
	chef.add_recipe "ohai"
  chef.add_recipe "supervisor"
  chef.add_recipe "postgresql::server"
  chef.add_recipe "bankstuff_srv"
	
	chef.json = {
		"apt" => {"compiletime" => true },
		"postgresql" => {
			"password" => {
				"postgres" => 'md5a61e95fb06599f2edbba3f196f738bab' # postgres/postgresq!
			},
			#"config" => {
			#	"listen_addresses" => "*"
			#},
			#pg_hba: [
				#{ type: 'local', db: 'all', user: 'postgres', method: 'ident' },
				#{ type: 'local', db: 'all', user: 'all', addr: '', method: 'trust' },
				#{type: 'host',  db: 'all', user: 'all',        addr: '192.168.248.1/24', method: 'md5'}
			#]
		}
	}
    #chef.run_list = [		
	  #'recipe[postgresql::server]',
      #'recipe[bankstuff_srv::default]',
	  #'recipe[python]',
    #]
  end

end
