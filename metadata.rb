name             'bankstuff_srv'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures bankstuff_srv'
long_description 'Installs/Configures bankstuff_srv'

version          '0.1.0'

depends "apt"
depends "build-essential"
depends "database"
depends "postgresql"
depends "python"
depends "git"
depends "nginx"
depends "uwsgi"
depends "pyenv"
depends "supervisor"
depends 'magic_shell'